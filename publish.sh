#!/bin/bash
gnome-extensions pack --force  .
zip -q custom-vpn-toggler@giteduberger.fr.shell-extension.zip  LICENSE images/icon.png images/security-high-symbolic.svg images/security-medium-symbolic.svg images/security-low-symbolic.svg                        . 
unzip -l custom-vpn-toggler@giteduberger.fr.shell-extension.zip
echo
echo -n "Version "
cat metadata.json | jq -r '.version'
echo "is ready to be published on https://extensions.gnome.org/upload/"
gnome-extensions install --force custom-vpn-toggler@giteduberger.fr.shell-extension.zip